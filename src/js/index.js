
// import {tool,util} from './common.js' // 不支持
let {tool,util} = require('./common.js')
let num = 0;
console.log(num);
console.log(tool,util);

const minus = (a = 10, b = 5) => {
  return a - b;
}
console.log(minus());
let age = 30;
let obj = {
  name: '李四',
  age: () => {
    return age;
  },
  info() {
    return `姓名:${this.name}
    年龄:${this.age()}
    `
  }
}
console.log(obj.info());

Promise.resolve(1)

var EventUtil = {
  addHandler: function (element, type, handler) {
      if (element.addEventListener) {
          element.addEventListener(type, handler, false);
      } else if (element.attachEvent) {
          element.attachEvent("on" + type, handler);
      } else {
          element["on" + type] = handler;
      }
  }
};
let eventHandler = Object.assign({},EventUtil,{
  ...tool
});
console.log("eventHandler",eventHandler);

$(function(){
  $(window).on('pageshow',()=>{
    console.log('pageshow',$('input'));
    $('input').val('');
  })
  // EventUtil.addHandler(window,'pageshow',(e)=>{
  //   e.sto//阻止默认事件
  //   //preventDefault()[dom标准写法(ie678不兼容)]
  //   //ie678用returnValue
  //   //或者利用return false也能阻止默认行为,没有兼容问题(只限传统注册方式)
  //   preventDefault()
  //   e.preventDefault()
  //   $('input').val('');
  // })
})
